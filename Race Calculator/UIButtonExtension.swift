//
//  UIButtonExtension.swift
//  Race Calculator
//
//  Created by Jonathan Roszkowski on 12/30/18.
//  Copyright © 2018 Jonathan Roszkowski. All rights reserved.
//

import Foundation
import UIKit

//Creates spring animation
extension UIButton {
  
  func pulsate() {
    
    let pulse = CASpringAnimation(keyPath: "transform.scale")
    pulse.duration = 0.3
    pulse.fromValue = 0.98
    pulse.toValue = 1.0
    pulse.autoreverses = true
    pulse.repeatCount = 1.2
    pulse.initialVelocity = 0.5
    pulse.damping = 1
    
    layer.add(pulse, forKey: nil)
  }
}


//Creates rounded button
class RoundedButton: UIButton {
  override func awakeFromNib() {
    super.awakeFromNib()
    layer.borderWidth = 2.0
    layer.borderColor = UIColor.blue.cgColor
    layer.cornerRadius = frame.size.height/2
  }
}

  
//Creates rounded button and content changing
class LightModeButtonToggle: UIButton {

  var isOn = false
  override init(frame: CGRect) {
    super.init(frame:frame)
    initButton()
  }
  
  required init?(coder aDecorder: NSCoder) {
    super.init(coder: aDecorder)
    initButton()
  }
  
  //Button color and corner radius
  func initButton (){
    layer.borderWidth = 2.0
    layer.borderColor = UIColor.blue.cgColor
    layer.cornerRadius = frame.size.height/2
    
    setTitleColor(UIColor.blue, for: .normal)
    addTarget(self, action: #selector(LightModeButtonToggle.buttonPressed), for: .touchUpInside)
  }
  
  //Defines what happens when button is pressed
  @objc func buttonPressed(){
    activateButton(bool: !isOn)
    
  }
  
  func activateButton(bool: Bool){
    
    isOn = bool
    
    let color = bool ? UIColor.blue : .white
    let title = bool ? "Enable Dark Mode" : "Enable Light Mode"
    let titleColor = bool ? .white : UIColor.blue
    
    setTitle(title, for: .normal)
    setTitleColor(titleColor, for: .normal)
    backgroundColor = color
  }
}

