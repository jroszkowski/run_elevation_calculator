//
//  PopUpViewController.swift
//  Race Calculator
//
//  Created by Jonathan Roszkowski on 12/28/18.
//  Copyright © 2018 Jonathan Roszkowski. All rights reserved.
//

import UIKit

class PopUpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
  }
  
  @IBAction func close(){
    dismiss(animated: true, completion: nil)
  }

}
